# project-0-part-2

part 2 of project 0 implementing data persistence using postgresql database with JDBC and an oracle DB, using the DAO design pattern. Test using J-unit and Log all transactions using Log4j.

insert the tables from create_table.txt into your database using debeaver, then insert your database url, username, and password into each daoimpl file