package com.RestaurantServiceTest;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.main.model.Refrigerator;
import com.main.service.RestaurantServiceImpl;



public class RestaurantServiceTest {
	public static String url = "jdbc:postgresql://datachan.c7nzvj5h5rkn.us-west-1.rds.amazonaws.com/Project-0";
	public static String username= "datachan";
	public static String password="p4ssw0rd";
	RestaurantServiceImpl service = new RestaurantServiceImpl();
	@BeforeClass
	public static void runOnce() {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "DELETE FROM Restaurant WHERE restaurant_name = 'dad'";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "DELETE FROM fridge_health_inspector_junction WHERE inspector_name = 'evan'";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
	 }
	
	
	@Test
	public void createAccountTest() {
		//assert true for name that hasn't been created
		
		assertTrue(service.createAccount("dad","dad"));
		
		//assert false for name that has been created
		
		assertFalse(service.createAccount("dad", "dad"));
		
		
	}
	
	@Test
	public void signinTest() {
		
		//has been created, return true
		
		assertTrue(service.signin("james", "james"));
		
		//hasn't been created, return false
		
		assertFalse(service.signin("wfewf", "asdfsfds"));
		
		//correct user name, wrong password returns false
		
		assertFalse(service.signin("james", "asdfsfds"));
		
		
	}
	
	
	@Test
	public void assignInspectorTest() {
		//return true if inspector hasn't been assigned to fridge
		assertTrue(service.assignInspector(6, "evan"));
		
		//return false if inspector is already inspecting fridge
		assertFalse(service.assignInspector(6, "evan"));
		
	}
	
	@Test
	public void addRefrigeratorTest() {
		ArrayList<Refrigerator> fridges = new ArrayList<Refrigerator>();
		ArrayList<Refrigerator> fridgesafteradd = new ArrayList<Refrigerator>();
		fridgesafteradd.add(new Refrigerator());
		fridges = service.addRefrigerator(fridges, username);
		assertTrue(fridges.equals(fridges));
	}
	
	@Test
	public void removeRefrigeratorTest() {
		ArrayList<Refrigerator> fridges = new ArrayList<Refrigerator>();
		ArrayList<Refrigerator> fridgesafterremove = service.removeRefrigerator(fridges, 0);
		assertTrue(fridges.equals(fridgesafterremove));
		fridges.add(new Refrigerator());
		fridges.add(new Refrigerator());
		fridges.get(0).food.add("lemonade");
		fridges.get(0).food.add("pizza");
		fridges.get(1).food.add("cauliflower");
		fridges.get(1).food.add("goodstuff");
		fridgesafterremove.add(new Refrigerator());
		fridgesafterremove.get(0).food.add("lemonade");
		fridgesafterremove.get(0).food.add("pizza");
		fridgesafterremove.get(0).food.add("cauliflower");
		fridgesafterremove.get(0).food.add("goodstuff");
		fridges = service.removeRefrigerator(fridges, 0);
		assertTrue(fridges.equals(fridgesafterremove));
	}
	


}













