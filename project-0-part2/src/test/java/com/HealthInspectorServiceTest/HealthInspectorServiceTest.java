package com.HealthInspectorServiceTest;




import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.main.model.Refrigerator;
import com.main.service.HealthInspectorServiceImpl;



public class HealthInspectorServiceTest {
	public static String url = "jdbc:postgresql://datachan.c7nzvj5h5rkn.us-west-1.rds.amazonaws.com/Project-0";
	public static String username= "datachan";
	public static String password="p4ssw0rd";
	HealthInspectorServiceImpl service = new HealthInspectorServiceImpl();
	@BeforeClass
	public static void runOnce() {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "DELETE FROM health_inspector WHERE inspector_name = 'newhealthinspector'";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS); 
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
	 }
	
	
	@Test
	public void createAccountTest() {
		//assert true for name that hasn't been created
		
		assertTrue(service.createAccount("newhealthinspector","newhealthinspector"));
		
		//assert false for name that has been created
		
		assertFalse(service.createAccount("newhealthinspector", "newhealthinspector"));
		
		
	}
	
	@Test
	public void signinTest() {
		
		//has been created, return true
		
		assertTrue(service.signin("evan", "moto"));
		
		//hasn't been created, return false
		
		assertFalse(service.signin("wfewf", "asdfsfds"));
		
		//correct user name, wrong password returns false
		
		assertFalse(service.signin("evan", "asdfsfds"));
		
		
	}
	
	
	@Test
	public void loadAllFridgesTest() {
		ArrayList<Refrigerator> fridges = new ArrayList<Refrigerator>();
		ArrayList<String> food = new ArrayList<String>();
		food.add("lemonade");
		food.add("lettuce");
		food.add("pizza");
		food.add("pizza");
		fridges.add(new Refrigerator(9, "james", food));
		assertTrue(fridges.equals(service.loadAllFridges("evan")));
		
		
	}
}