package com.main.dao;
import java.util.ArrayList;
public interface FridgeInspectorJunctionDao {
	public void create(int fridgeid, String inspector);
	public ArrayList<String> getInspectors(int fridgeid);
	public ArrayList<Integer> getFridgesIds(String inspectors);
	public void deleteFridge(int fridgeid);
}
