package com.main.dao;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


//import com.main.model.Refrigerator;

public class FridgeInspectorJunctionDaoImpl implements FridgeInspectorJunctionDao {
	public static String url = "";
	public static String username= "";
	public static String password="";
	
	@Override
	public void create(int fridgeid, String inspector){
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "INSERT INTO Fridge_Health_Inspector_Junction VALUES(?,?)";	
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, fridgeid); 
			ps.setString(2, inspector);
			
			ps.executeUpdate(); 
			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public ArrayList<String> getInspectors(int fridgeid) {
		ArrayList<String> inspectorslist = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Fridge_Health_Inspector_Junction WHERE fridge_id =" + "'" +fridgeid+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				inspectorslist.add(rs.getString(2));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return inspectorslist;
	}
	

	@Override
	public ArrayList<Integer> getFridgesIds(String inspectors) {
		ArrayList<Integer> fridgeids = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Fridge_Health_Inspector_Junction WHERE inspector_name =" + "'" +inspectors+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				fridgeids.add(rs.getInt(1));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return fridgeids;
	}

	@Override
	public void deleteFridge(int fridgeid) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "DELETE FROM Fridge_Health_Inspector_Junction WHERE fridge_id = " + "'" +fridgeid+ "'";
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteByInspector(String inspector) {
try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "DELETE FROM Fridge_Health_Inspector_Junction WHERE inspector_name = " + "'" +inspector+ "'";
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

}
