package com.main.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HealthInspectorDaoImpl implements HealthInspectorDao {
	public static String url = "";
	public static String username= "";
	public static String password="";
	@Override
	public void create(String newusername, String newpassword) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "INSERT INTO Health_Inspector VALUES(?, ?)";
			
															
			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, newusername); 
			ps.setString(2, newpassword); 
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String[] get(String myusername, String mypassword) {
		String[] returneditems = new String[2];
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Health_Inspector WHERE inspector_name = "+ "'" +myusername+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				returneditems[0] = rs.getString(1);
				returneditems[1] = rs.getString(2);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return returneditems;
	}
	
	public void printInspectors() {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT inspector_name FROM Health_Inspector";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getString(1));
			}
			
			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

}
