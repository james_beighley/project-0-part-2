package com.main.dao;

import java.util.ArrayList;
import com.main.model.Refrigerator;

public interface RefrigeratorDao {
	public void create(Refrigerator newfridge); //create Fridge
	public ArrayList<Refrigerator> getAll(String restaurant); //get existing Fridges from table 
															  //owned by restaurant
	public Refrigerator getById(int fridgeid);//get single refrigerator by its id
	public void delete(int id);//delete fridge from table
}
