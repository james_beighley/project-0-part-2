package com.main.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import com.main.model.Refrigerator;

public class RefrigeratorDaoImpl implements RefrigeratorDao {
	
	public static String url = "";
	public static String username= "";
	public static String password="";

	@Override
	public void create(Refrigerator newfridge) {
			try(Connection conn = DriverManager.getConnection(url, username, password)){
				String sql;
				if(newfridge.id==0) {
					sql = "INSERT INTO Fridge VALUES(DEFAULT,?,?,?,?,?,?)";
					String name = newfridge.restaurant;
					String[] food = new String[5];
					for(int i = 0; i< 5; i++) {
						if(i<newfridge.food.size())
							food[i] = newfridge.food.get(i);
						else
							food[i] = null;
					}
					
																	//necessary to return the PK
					PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					ps.setString(1, name); //the restaurant_id
					ps.setString(2, food[0]); //the 2nd "?"
					ps.setString(3, food[1]); //the 3rd "?"
					ps.setString(4, food[2]); //the 4th "?"
					ps.setString(5, food[3]); //the 4th "?"
					ps.setString(6, food[4]); //the 4th "?"
					ps.executeUpdate();
				}
					
				else {
					sql = "INSERT INTO Fridge VALUES(?,?,?,?,?,?,?)";
					String name = newfridge.restaurant;
					String[] food = new String[5];
					for(int i = 0; i< 5; i++) {
						if(i<newfridge.food.size())
							food[i] = newfridge.food.get(i);
						else
							food[i] = null;
					}
					
																	//necessary to return the PK
					PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
					ps.setInt(1, newfridge.id);
					ps.setString(2, name); 
					ps.setString(3, food[0]); 
					ps.setString(4, food[1]); 
					ps.setString(5, food[2]); 
					ps.setString(6, food[3]); 
					ps.setString(7, food[4]); 
					
					ps.executeUpdate(); //THIS line is what sends the information to the DB
					
				}
				
				
			
			}catch(SQLException e) {
				e.printStackTrace();
			}

	}

	@Override
	public ArrayList<Refrigerator> getAll(String restaurant) {
		ArrayList<Refrigerator> fridges = new ArrayList<>();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Fridge WHERE restaurant_id =" + "'" +restaurant+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				ArrayList<String> tempList = new ArrayList<String>();
				int i = 3;
				while(i<8) {
					if(rs.getString(i)!=null)
						tempList.add(rs.getString(i));
					i++;
				}
				fridges.add(new Refrigerator(rs.getInt(1), rs.getString(2), tempList));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		return fridges;

	}
	
	public Refrigerator getById(int fridgeid) {
		Refrigerator toreturn = new Refrigerator();
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Fridge WHERE fridge_id =" + "'" +fridgeid+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				ArrayList<String> tempList = new ArrayList<String>();
				int i = 3;
				while(i<8 && (rs.getString(i)!=null)) {
					tempList.add(rs.getString(i));
					i++;
				}
				toreturn = new Refrigerator(rs.getInt(1), rs.getString(2), tempList);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	
		return toreturn;

	}


	@Override
	public void delete(int id) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "DELETE FROM Fridge WHERE fridge_id = " + "'" +id+ "'";
			
			Statement statement = conn.createStatement();
			statement.executeUpdate(sql);

			
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

}
