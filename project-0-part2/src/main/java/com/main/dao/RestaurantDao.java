package com.main.dao;

public interface RestaurantDao {
	public void create(String username, String password); //create restaurant
	public String[] get(String username, String password); //get restaurant from list
	
}
