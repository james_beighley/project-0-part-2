package com.main.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RestaurantDaoImpl implements RestaurantDao {
	public static String url = "";
	public static String username= "";
	public static String password="";
	@Override
	public void create(String restaurant_name, String restaurant_password) {
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			

			
			String sql = "INSERT INTO restaurant (restaurant_name, restaurant_password) VALUES(?, ?)";
			

			PreparedStatement ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, restaurant_name); 
			ps.setString(2, restaurant_password); 
			
			ps.executeUpdate(); 
		
		}catch(SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public String[] get(String myusername, String mypassword) {
		String[] returneditems = new String[2];
		
		try(Connection conn = DriverManager.getConnection(url, username, password)){
			
			String sql = "SELECT * FROM Restaurant WHERE restaurant_name = "+ "'" +myusername+ "'";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery(); //<--query not update
			
			while(rs.next()) {
				returneditems[0] = rs.getString(1);
				returneditems[1] = rs.getString(2);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println(returneditems);
		return returneditems;
	}


}
