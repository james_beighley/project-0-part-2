package com.main;

import org.apache.log4j.Level;
import java.util.Scanner;
import org.apache.log4j.Logger;
import com.main.model.HealthInspector;
import com.main.service.HealthInspectorServiceImpl;


public class InspectorMenu {
	static Scanner userinput = new Scanner(System.in);
	final static Logger loggy = org.apache.log4j.Logger.getLogger(MainDriver.class);
	static HealthInspectorServiceImpl service = new HealthInspectorServiceImpl();
	public static void InspectorStart() {
		loggy.setLevel(Level.ALL);
		if(loggy.isInfoEnabled())
			loggy.info("Inspector signing in");
		System.out.println("enter '1' to login, '2' to make a new account, anything else to quit");
		String choice = userinput.nextLine();
		
		if(choice.equals("1")) {
			login();
		}
		else if(choice.equals("2")) {
			signup();
		}
		else
			System.out.println();
	}

	private static void login() {
		System.out.println("Please enter your username:");
		String username = userinput.nextLine();
		System.out.println("Please enter your password:");
		String password = userinput.nextLine();
		if(service.signin(username, password))
			insideProgram(username, password);
		else
			System.out.println("not a valid username, check your spelling or create a new account.");
	}
	
	private static void signup() {
		System.out.println("Please enter a new username(no ':' or '-'):");
		String username = userinput.nextLine();
		System.out.println("Please enter a new password:");
		String password = userinput.nextLine();
		if(service.createAccount(username, password))
			insideProgram(username, password);
		else
			System.out.println("Username is taken. Run program again and choose a new one.");
		
	}
	
	private static void insideProgram(String username, String password) {
		HealthInspector myhealthinspector = new HealthInspector(username);
		myhealthinspector.fridges = service.loadAllFridges(username);
		boolean stay = true;
		if(myhealthinspector.fridges.size()==0) {
			System.out.println("You were not assigned any fridges! Come back when you have some!");
			stay = false;
		}
		while(stay) {
			myhealthinspector.fridges = service.loadAllFridges(username);
			myhealthinspector.printFridges();
			System.out.println("enter the (key) to invoke the command");
			System.out.println("(1) remove food to fridge");
			String input = userinput.nextLine();
			switch(input) {
				case "1":
					System.out.println("Select a fridge to withdraw food from (0 to " + (myhealthinspector.fridges.size()-1) + "):");
					int fridgetowithdraw = userinput.nextInt();
					userinput.nextLine();
					myhealthinspector.fridges.get(fridgetowithdraw).printFood();
					System.out.println("Type one of the above options to withdraw");
					String foodtowithdraw = userinput.nextLine();
					myhealthinspector.fridges = service.removeFood(myhealthinspector.fridges, fridgetowithdraw, foodtowithdraw);
					loggy.info(foodtowithdraw + " has been withdrawn from fridge" + myhealthinspector.fridges.get(fridgetowithdraw).id);
					break;
				default:
					stay = false;
					System.out.println("signed out");
			}
			service.saveAllChanges(myhealthinspector.fridges, myhealthinspector.username);
		}
	}
	
}
