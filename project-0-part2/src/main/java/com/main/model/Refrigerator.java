package com.main.model;

import java.util.ArrayList;

public class Refrigerator {
	public int id;
	public String restaurant;
	public ArrayList<String> food;
	public Refrigerator(int id, String restaurant, ArrayList<String> otherfood){
		this.id = id;
		this.restaurant = restaurant;
		food = new ArrayList<String>();
		for(int i = 0; i< otherfood.size(); i++) {
			food.add(otherfood.get(i));
		}
	}
	
	public Refrigerator(String restaurant, ArrayList<String> otherfood){
		this.id = 0;
		this.restaurant = restaurant;
		food = new ArrayList<String>();
		for(int i = 0; i< otherfood.size(); i++) {
			food.add(otherfood.get(i));
		}
	}
	
	public Refrigerator(String restaurant){
		this.id = 0;
		this.restaurant = restaurant;
		food = new ArrayList<String>();
	}
	
	public Refrigerator(){
		this.id = 0;
		this.restaurant = "";
		food = new ArrayList<String>();
	}
	
	public void printFood() {
		for(int i = 0; i<food.size(); i++) {
			System.out.print(i + ": " + food.get(i) + "  ");
		}
	}
	
}
