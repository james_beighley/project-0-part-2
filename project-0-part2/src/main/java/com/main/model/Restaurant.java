package com.main.model;

import java.util.ArrayList;

public class Restaurant extends Account {
	
	public Restaurant(String username, String password) {
		this.username = username;
		this.password = password;
		this.fridges = new ArrayList<Refrigerator>();
	}
	
	public void printFridges() {
		System.out.println(username);
		for(int i = 0; i< fridges.size(); i++) {
			System.out.print("Fridge " + i + ": ");
			fridges.get(i).printFood();
			System.out.println();
		}
	}
	
}
