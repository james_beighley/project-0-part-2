package com.main.model;

import java.util.ArrayList;

public class HealthInspector extends Account {
	public HealthInspector(String username) {
		this.username = username;
		fridges = new ArrayList<Refrigerator>();
	}
	public void printFridges() {
		for(int i = 0; i< fridges.size(); i++) {
			System.out.print("Fridge " + i + ": ");
			fridges.get(i).printFood();
			System.out.println();
		}
		
	}
}
