package com.main.service;

import java.util.ArrayList;
import com.main.model.Refrigerator;


public interface HealthInspectorService {
	public void saveAllChanges(ArrayList<Refrigerator> fridge, String inspector);
	public ArrayList<Refrigerator> loadAllFridges(String inspector);
	public boolean createAccount(String username, String password);
	public boolean signin(String username, String password);
}
