package com.main.service;
import com.main.dao.*;
import java.util.ArrayList;
import com.main.model.Refrigerator;

public class HealthInspectorServiceImpl implements HealthInspectorService {
	HealthInspectorDaoImpl inspectordao = new HealthInspectorDaoImpl();
	RefrigeratorDaoImpl fridgedao = new RefrigeratorDaoImpl();
	FridgeInspectorJunctionDaoImpl junctiondao = new FridgeInspectorJunctionDaoImpl();
	@Override
	public void saveAllChanges(ArrayList<Refrigerator> fridges, String inspector) {
		for(int i = 0; i< fridges.size(); i++) {
			fridgedao.delete(fridges.get(i).id);
		}
		for(int i = 0; i<fridges.size(); i++) {
			fridgedao.create(fridges.get(i));
		}

	}

	@Override
	public ArrayList<Refrigerator> loadAllFridges(String inspector) {
		ArrayList<Integer> fridgeids = junctiondao.getFridgesIds(inspector);
		ArrayList<Refrigerator> fridgelist = new ArrayList<Refrigerator>();
		for(int i = 0; i< fridgeids.size(); i++) {
			fridgelist.add(fridgedao.getById(fridgeids.get(i)));
		}
		return fridgelist;

	}


	@Override
	public boolean createAccount(String username, String password) {
		String[] found = inspectordao.get(username, password);
		if(found[0]!=null)
			return false;
		else
			inspectordao.create(username, password);
		return true;
	}

	@Override
	public boolean signin(String username, String password) {
		String[] found = inspectordao.get(username, password);
		if(found[0] == null) 
			return false;
		else if(found[0].equals(username) && found[1].equals(password))
				return true;
		
		return false;
	}
	
	public ArrayList<Refrigerator> removeFood(ArrayList<Refrigerator> fridges, int refrigerator, String food) {
		for(int i = 0; i< fridges.get(refrigerator).food.size(); i++) {
			if(fridges.get(refrigerator).food.get(i).equals(food)) {
				fridges.get(refrigerator).food.remove(i);
				break;
			}
				
		}
		return fridges;
	}
	
	public void printInspectors() {
		inspectordao.printInspectors();
	}

}
