package com.main.service;

import com.main.dao.*;
import java.util.ArrayList;
import com.main.model.Refrigerator;

public class RestaurantServiceImpl implements RestaurantService {

	RestaurantDaoImpl restaurantdao = new RestaurantDaoImpl();
	RefrigeratorDaoImpl fridgedao = new RefrigeratorDaoImpl();
	FridgeInspectorJunctionDaoImpl junctiondao = new FridgeInspectorJunctionDaoImpl();
	HealthInspectorDaoImpl inspectordao = new HealthInspectorDaoImpl();
	@Override
	public void saveAllChanges(ArrayList<Refrigerator> fridges) {
		for(int i = 0; i<fridges.size(); i++) {
			fridgedao.create(fridges.get(i));
		}

	}

	@Override
	public ArrayList<Refrigerator> loadAllFridges(String restaurant) {
		ArrayList<Refrigerator> fridges = fridgedao.getAll(restaurant);
		for(int i = 0; i< fridges.size(); i++) {
			fridgedao.delete(fridges.get(i).id);
		}
		return fridges;

	}


	@Override
	public boolean createAccount(String username, String password) {
		String[] found = restaurantdao.get(username, password);
		if(found[0]!=null)
			return false;
		else
			restaurantdao.create(username, password);
		return true;

		
	}

	@Override
	public boolean signin(String username, String password) {
		String[] found = restaurantdao.get(username, password);
		if(found[0] == null) 
			return false;
		else if(found[0].equals(username) && found[1].equals(password))
				return true;
		
		return false;
	}
	
	public boolean assignInspector(int fridgeid, String inspector) {
		ArrayList<String> assignedinspectors = junctiondao.getInspectors(fridgeid);
		for(int i = 0; i< assignedinspectors.size(); i++) {
			if(inspector.equals(assignedinspectors.get(i))) {
				return false;
			}
		}
		junctiondao.create(fridgeid, inspector);
		return true;
	}
	
	public void removeFromJunction(int fridgeid) {
		junctiondao.deleteFridge(fridgeid);
	}
	
	public void printInspectors() {
		inspectordao.printInspectors();
	}
	
	public ArrayList<Refrigerator> addRefrigerator(ArrayList<Refrigerator> fridges, String username) {
		fridges.add(new Refrigerator(username));
		return fridges;
	}
	
	public ArrayList<Refrigerator> removeRefrigerator(ArrayList<Refrigerator> fridges, int index) {
		if(fridges.size()<=1) {
			System.out.println("not enough fridges to remove, need 2 or more.");
			return fridges;
		}
		if(index<0 || index>fridges.size()-1) {
			System.out.println("invalid index.");
			return fridges;
		}
		else {
			Refrigerator removed = fridges.remove(index);
			int length = removed.food.size();
			if(length==0)
				System.out.println("Empty fridge removed, no Food lost.");
			else {
				for(int i = 0; i<fridges.size(); i++) {
					for(int j = fridges.get(i).food.size()-1; j<5; j++) {
							fridges.get(i).food.add(removed.food.remove(0));
							length--;
							if(length==0)
								break;
					}
					if(length==0)
						break;
				}
				if(length==0) {
					System.out.println("Fridge was removed and all Food inside was moved to other fridges");
				}
				else {
					System.out.println("Fridge was removed but these items were thrown away:");
					for(int i = length-1; i>=0; i--) {
						System.out.println(removed.food.get(i));
					}
				}
			}
			return fridges;
		}
	}
	
	public ArrayList<Refrigerator> depositFood(ArrayList<Refrigerator> fridges, int refrigerator, String food) {
		if(fridges.get(refrigerator).food.size() < 5) {
			fridges.get(refrigerator).food.add(food);
			System.out.println(food + " successfully added to fridge" + refrigerator);	
		}
		else {
			System.out.println("selected fridge is full");
		}
		return fridges;
		
	}
	
	public ArrayList<Refrigerator> withdrawFood(ArrayList<Refrigerator> fridges, int refrigerator, String food) {
		String found = "nothing";
		for(int i = 0; i< fridges.get(refrigerator).food.size(); i++) {
			if(fridges.get(refrigerator).food.get(i).equals(food))
				found = fridges.get(refrigerator).food.remove(i);
		}
		System.out.println("removed " +found+ "from fridge " + refrigerator);
		return fridges;
	}
	
	public ArrayList<Refrigerator> transferFood(ArrayList<Refrigerator> fridges, int from, int to, String food) {
		String found = "nothing";
		for(int i = 0; i< fridges.get(from).food.size(); i++) {
			if(fridges.get(from).food.get(i).equals(food))
				found = fridges.get(from).food.remove(i);
		}
		if(found.equals("nothing")) {
			System.out.println(food + " not found in fridge " + from);
			return fridges;
		}
		if(fridges.get(to).food.size() < 5) {
			fridges.get(to).food.add(food);
			System.out.println(food + " successfully added to fridge" + to);	
		}
		else {
			System.out.println("selected fridge is full");
			System.out.println("returning " + food + " to original fridge" + from);
			fridges.get(from).food.add(food);
		}
		return fridges;
	}

}
