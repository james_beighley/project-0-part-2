package com.main.service;

import java.util.ArrayList;

import com.main.model.Refrigerator;

public interface RestaurantService {
	public void saveAllChanges(ArrayList<Refrigerator> fridge);
	public ArrayList<Refrigerator> loadAllFridges(String inspector);
	public boolean createAccount(String username, String password);
	public boolean signin(String username, String password);
	public boolean assignInspector(int fridgeid, String inspector);

}
