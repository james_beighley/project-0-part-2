package com.main;

import org.apache.log4j.Level;
import java.util.Scanner;
import org.apache.log4j.Logger;
import com.main.model.Restaurant;
import com.main.service.RestaurantServiceImpl;

import java.util.ArrayList;
import com.main.model.Refrigerator;
public class MainDriver {
	final static Logger loggy = org.apache.log4j.Logger.getLogger(MainDriver.class);
	static Scanner userinput = new Scanner(System.in);
	static RestaurantServiceImpl service = new RestaurantServiceImpl();
	
	
	public static void main(String[] args) {
		loggy.setLevel(Level.ALL);
		if(loggy.isInfoEnabled())
			loggy.info("Restaurant Fridge app has started");
		System.out.println("Welcome to Fridge Management inc.");
		System.out.println("enter '1' if you are a restaurant, ");
		System.out.println("'2' if you are a health inspector,");
		System.out.println("anything else to quit.");
		String choice = userinput.nextLine();
		
		if(choice.equals("2")) {
			InspectorMenu.InspectorStart();
		}
		else if(choice.equals("1")) {
			System.out.println("enter '1' to login, '2' to make a new account, anything else to quit");
			choice = userinput.nextLine();
			if(choice.equals("1"))
				login();
			else if(choice.equals("2")) 
				signup();
			else {
				
			}
				
		}
		else {
			
		}
		System.out.println("exiting program");

	}
	
	
	private static void login() {
		System.out.println("Please enter your username:");
		String username = userinput.nextLine();
		System.out.println("Please enter your password:");
		String password = userinput.nextLine();
		if(service.signin(username, password))
			insideProgram(username, password);
		else
			System.out.println("not a valid username, check your spelling or create a new account.");
		
	}
	
	private static void signup() {
		System.out.println("Please enter a new username(no ':' or '-'):");
		String username = userinput.nextLine();
		System.out.println("Please enter a new password:");
		String password = userinput.nextLine();
		if(service.createAccount(username, password)) {
			insideProgram(username, password);
		}
		else {
			System.out.println("Username was already taken");
		}
		
	}
	
	private static void insideProgram(String username, String password) {
		Restaurant myrestaurant = new Restaurant(username, password);
		boolean stay = true;
		while(stay) {
			myrestaurant.fridges = service.loadAllFridges(username);
			myrestaurant.printFridges();
			System.out.println("enter the (key) to invoke the command");
			System.out.println("(1) add refrigerator");
			System.out.println("(2) remove refrigerator");
			System.out.println("(3) deposit food to fridge");
			System.out.println("(4) withdraw food from fridge");
			System.out.println("(5) transfer food from one fridge to another");
			System.out.println("(6) assign inspector to fridge");
			System.out.println("(any other key) save and quit");
			String input = userinput.nextLine();
			switch(input) {
				case "1":
					loggy.info("Restaurant has been added");
					System.out.println("Adding refrigerator");
					myrestaurant.fridges=service.addRefrigerator(myrestaurant.fridges, username);
					break;
				case "2":
					if(myrestaurant.fridges.size()<2)
						System.out.println("you need 2 or more fridges to delete one");
					else {
						System.out.println("Select the fridge to remove (0 to " + (myrestaurant.fridges.size()-1) + "):");
						int fridgetoremove = userinput.nextInt();
						int id = myrestaurant.fridges.get(fridgetoremove).id;
						userinput.nextLine();
						service.removeFromJunction(id);
						loggy.info("Fridge has been removed");
						myrestaurant.fridges = service.removeRefrigerator(myrestaurant.fridges, fridgetoremove);
					}
					
					
					break;
				case "3":
					System.out.println("Select a fridge to deposit food into (0 to " + (myrestaurant.fridges.size()-1) + "):\"");
					int fridgetodeposit = userinput.nextInt();
					userinput.nextLine();
					System.out.println("Name a food to put inside of the fridge");
					String foodtodeposit = userinput.nextLine();
					//userinput.nextLine();
					if(myrestaurant.fridges.get(fridgetodeposit).food.size()<5) {
						loggy.info(foodtodeposit + "has been deposited to fridge " + fridgetodeposit);
						myrestaurant.fridges = service.depositFood(myrestaurant.fridges, fridgetodeposit, foodtodeposit);
					}
					
					break;
				case "4":
					System.out.println("Select a fridge to withdraw food from (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetowithdraw = userinput.nextInt();
					userinput.nextLine();
					myrestaurant.fridges.get(fridgetowithdraw).printFood();
					System.out.println("Type one of the above options to withdraw");
					String foodtowithdraw = userinput.nextLine();
					int originalsize = myrestaurant.fridges.get(fridgetowithdraw).food.size();
					myrestaurant.fridges = service.withdrawFood(myrestaurant.fridges, fridgetowithdraw, foodtowithdraw);
					if(originalsize!=myrestaurant.fridges.get(fridgetowithdraw).food.size())
						loggy.info(foodtowithdraw + " has been withdrawn from fridge" + myrestaurant.fridges.get(fridgetowithdraw).id);
					else
						System.out.println("Fridge was empty or didn't have selected item inside");
					
					break;
				case "5":
					System.out.println("Select a fridge to transfer food from (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetotransferfrom = userinput.nextInt();
					userinput.nextLine();
					myrestaurant.fridges.get(fridgetotransferfrom).printFood();
					System.out.println();
					System.out.println("Type one of the above options to withdraw (the word not the number)");
					String foodtotransfer = userinput.nextLine();
					System.out.println("Select a fridge to transfer food into (0 to " + (myrestaurant.fridges.size()-1) + "):\"");
					int fridgetotransferto = userinput.nextInt();
					userinput.nextLine();
					if(fridgetotransferfrom==fridgetotransferto){
						System.out.println("error: the two fridges cannot be the same");
						break;
					}
					int firstsize = myrestaurant.fridges.get(fridgetotransferfrom).food.size();
					myrestaurant.fridges = service.transferFood(myrestaurant.fridges, fridgetotransferfrom, fridgetotransferto, foodtotransfer);
					if(firstsize!=myrestaurant.fridges.get(fridgetotransferfrom).food.size())
						loggy.info(foodtotransfer + " has been has been transferred from fridge" + fridgetotransferfrom + "to" + fridgetotransferto);

					break;
				case "6":
					System.out.println("Select a fridge to assign an inspector to (0 to " + (myrestaurant.fridges.size()-1) + "):");
					int fridgetoassigninspector = userinput.nextInt();
					userinput.nextLine();
					System.out.println("select an inspector from the list to assign to fridge" + fridgetoassigninspector);
					service.printInspectors();
					String inspector = userinput.nextLine();
					if(service.assignInspector(myrestaurant.fridges.get(fridgetoassigninspector).id, inspector)){
						loggy.info(inspector + "assigned to fridge!");
					}
					else {
						System.out.println("Inspector doesn't exist or is already assigned to fridge");
					}
					break;
				default:
					stay = false;
					System.out.println("signed out");
			}
			service.saveAllChanges(myrestaurant.fridges);
		}
		
		
	}


}
